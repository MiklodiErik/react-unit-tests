import { useState } from "react";

export default function Hello() {
  const [ChangeText, setChangeText] = useState(false);

  function changeTextHandler() {
    setChangeText(true);
  }

  return (
    <div>
      <h2 data-testid="hello">Hello World!</h2>

      {ChangeText ? <p>Im ok</p> : <p>How are you?</p>}

      <button onClick={changeTextHandler}>Change the text</button>
    </div>
  );
}
