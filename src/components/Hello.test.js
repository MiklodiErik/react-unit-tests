import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import Hello from "./Hello";

describe("Hello component", () => {
  test("renders 'Hello World'", () => {
    // Arrange
    render(<Hello />);

    // Act -> nothing...

    // Assert
    const helloWorldElement = screen.getByTestId("hello");
    expect(helloWorldElement).toBeInTheDocument();
  });

  test("renders 'im ok' if the button was clicked", () => {
    render(<Hello />);

    const buttonElement = screen.getByRole("button");
    userEvent.click(buttonElement);

    const el = screen.getByText("Im ok", { exact: false });
    expect(el).toBeInTheDocument();
  });
});
