import logo from "./logo.svg";
import "./App.css";
import Hello from "./components/Hello";
import Async from "./components/Async";

function App() {
  return (
    <div className="App">
      <Hello />
      <Async />
    </div>
  );
}

export default App;
